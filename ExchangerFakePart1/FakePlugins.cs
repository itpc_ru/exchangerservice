﻿using System.ComponentModel.Composition;

namespace RICExchangerFakePart1
{
    public class FakePlugins
    {
        [Export("ScheduleMethod")]
        public string Method1()
        {
            return "Method1";
        }

        [Export("ScheduleMethod")]
        public string Method2()
        {
            return "Method2";
        }
    }
}
