﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExchangerService.Tests
{
    [TestClass]
    public class ExchangerTests
    {
        Exchanger exchanger;
        TimerSample timer;
        EventLogSample eventLog;

        [TestInitialize]
        public void Setup()
        {
            timer = new TimerSample();
            eventLog = new EventLogSample();
            exchanger = (Exchanger)Exchanger.Create(eventLog, timer);
        }

        [TestMethod]
        public void Stop()
        {
            Assert.IsFalse(timer.IsStopped);
            exchanger.Stop();
            Assert.IsTrue(timer.IsStopped);
        }

        [TestMethod]
        public void RunByTimer()
        {
            Assert.IsFalse(timer.IsStarted);
            exchanger.RunByTimer();
            Assert.IsTrue(timer.IsStarted);
        }

        [TestMethod]
        public void Run()
        {
            DateTime now = new DateTime(2016, 1, 1, 23, 0, 1);
            exchanger.Run(now);
        }
    }
}
