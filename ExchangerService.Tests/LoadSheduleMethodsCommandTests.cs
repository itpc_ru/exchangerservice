﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace ExchangerService.Tests
{
    [TestClass]
    public class LoadSheduleMethodsCommandTests
    {
        private LoadSheduleMethodsCommand command;
        private FakeMainPart fakeMainPart;

        [TestInitialize]
        public void Setup()
        {
            fakeMainPart = new FakeMainPart();
            command = new LoadSheduleMethodsCommand(fakeMainPart);
        }

        [TestMethod]
        public void Execute()
        {
            Assert.IsNull(fakeMainPart.ScheduleMethods);

            command.Execute();

            Assert.AreEqual(2, fakeMainPart.ScheduleMethods.Count());
            var res1 = fakeMainPart.ScheduleMethods.ToList()[0]();
            var res2 = fakeMainPart.ScheduleMethods.ToList()[1]();
            Assert.AreEqual("Method1",res1);
            Assert.AreEqual("Method2", res2);
        }
    }
}
