﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExchangerService.Tests
{
    [TestClass]
    public class IsCanBeRunTasksTests
    {
        [TestMethod]
        public void Make_pollInterval_ok()
        {
            int pollInterval = 1000; // ms
            DateTime now = new DateTime(2016, 1, 1, 23, 0, 1);
            var res = IsCanBeRunTasks.Make(now, pollInterval);
            Assert.IsTrue(res);
        }

        [TestMethod]
        public void Make_pollInterval_less()
        {
            int pollInterval = 500; // ms
            DateTime now = new DateTime(2016, 1, 1, 23, 0, 1);
            var res = IsCanBeRunTasks.Make(now, pollInterval);
            Assert.IsFalse(res);
        }
    }
}
