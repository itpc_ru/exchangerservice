﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExchangerService.Tests
{
    [TestClass]
    public class ExchangerServiceTests
    {
        private ExchangerService service;
        private ExchangerSample exchanger;

        [TestInitialize]
        public void Setup()
        {
            exchanger = new ExchangerSample();
            service = new ExchangerService(exchanger);
        }

        [TestMethod]
        public void FakeInstance()
        {
            Assert.IsNotNull(service.RICExchanger);
        }

        [TestMethod]
        public void Create()
        {
            service = ExchangerService.Create();
            Assert.IsNotNull(service.RICExchanger);
        }

        [TestMethod]
        public void OnStart_success()
        {
            Assert.IsFalse(exchanger.IsRunByTimer);
            service.OnStart();
            Assert.IsTrue(exchanger.IsRunByTimer);
        }

        [TestMethod]
        public void OnStart_error()
        {
            exchanger.RaiseErrorInRunByTimer = true;
            Assert.IsFalse(exchanger.IsRunByTimer);
            Assert.IsFalse(exchanger.IsStopped);

            service.OnStart();

            Assert.IsFalse(exchanger.IsRunByTimer);

            Assert.IsTrue(exchanger.IsStopped);
        }

        [TestMethod]
        public void StopService()
        {
            Assert.IsFalse(exchanger.IsStopped);

            service.StopService();

            Assert.IsTrue(exchanger.IsStopped);
        }
    }
}
