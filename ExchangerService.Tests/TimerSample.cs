﻿using System.Timers;

namespace ExchangerService.Tests
{
    public class TimerSample : TimerBase
    {
        public event ElapsedEventHandler Elapsed;
        public bool IsStarted;
        public bool IsStopped;

        public void Start()
        {
            IsStarted = true;
        }

        public void Stop()
        {
            IsStopped = true;
        }

        public int PollInterval
        {
            get { return 3000; }
        }
    }
}
