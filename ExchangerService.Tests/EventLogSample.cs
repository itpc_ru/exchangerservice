﻿using System;

namespace ExchangerService.Tests
{
    public class EventLogSample : EventLog
    {
        public string Message;
        public void WriteEntry(string message)
        {
            Message += message + Environment.NewLine;
        }
    }
}
