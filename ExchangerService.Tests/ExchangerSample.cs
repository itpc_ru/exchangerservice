﻿using System;

namespace ExchangerService.Tests
{
    public class ExchangerSample : ExchangerBase
    {
        public bool IsRunByTimer;
        public bool IsStopped;
        public bool RaiseErrorInRunByTimer = false;

        public void RunByTimer()
        {
            if (RaiseErrorInRunByTimer)
                throw new Exception("Error in RunByTimer");
            IsRunByTimer = true;
        }

        public void Stop()
        {
            IsStopped = true;
        }
    }
}
