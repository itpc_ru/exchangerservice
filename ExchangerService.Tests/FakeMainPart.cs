﻿using System.Collections.Generic;
using System.ComponentModel.Composition;

namespace ExchangerService.Tests
{
    public class FakeMainPart
    {
        [ImportMany("ScheduleMethod")]
        public IEnumerable<ScheduleMethod> ScheduleMethods { get; set; }
    }
}
