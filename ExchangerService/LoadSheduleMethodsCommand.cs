﻿using ru.itpc.data;
using System;
using System.ComponentModel.Composition.Hosting;
using System.IO;
using System.Reflection;
using System.ComponentModel.Composition;

namespace ExchangerService
{
    public class LoadSheduleMethodsCommand : ICommand
    {
        private object part;
        public LoadSheduleMethodsCommand(object part)
        {
            this.part = part;
        }

        public void Execute()
        {
            AggregateCatalog catalog = new AggregateCatalog();
            catalog.Catalogs.Add(new DirectoryCatalog(
                Path.GetDirectoryName(
                    new Uri(Assembly.GetExecutingAssembly()
                                   .CodeBase).LocalPath)));

            var container = new CompositionContainer(catalog);
            container.SatisfyImportsOnce(this.part);
        }
    }
}
