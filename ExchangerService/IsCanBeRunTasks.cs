﻿using NCrontab;
using System;
using System.Configuration;

namespace ExchangerService
{
    public static class IsCanBeRunTasks
    {
        public static bool Make(DateTime now, int pollInterval)
        {
            string cronDateRunTasks = (string)ConfigurationManager.AppSettings["ScheduleRunTasks"];
            if (string.IsNullOrEmpty(cronDateRunTasks)) 
                return false;                
            var schedule = CrontabSchedule.Parse(cronDateRunTasks);
            DateTime runDate = schedule.GetNextOccurrence(now.AddDays(-1));

            return (now > runDate && now < runDate.AddMilliseconds(pollInterval * 2));
        }
    }
}
