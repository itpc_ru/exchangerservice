﻿using System.Timers;

namespace ExchangerService
{
    public class ServiceTimer : TimerBase
    {
        public event ElapsedEventHandler Elapsed;
        private Timer timer;

        public ServiceTimer(double interval)
        {
            timer = new Timer(interval);
        }

        public int PollInterval {
            get { return (int)timer.Interval; }
        }

        public void Start()
        {
            timer.Start();
        }

        public void Stop()
        {
            timer.Stop();
        }
    }
}
