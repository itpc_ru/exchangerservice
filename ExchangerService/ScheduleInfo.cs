﻿namespace ExchangerService
{
    public class ScheduleInfo
    {
        private string scheduleName;
        private string[] cronTabs;
        private ScheduleMethod scheduleMethod;

        public string ScheduleName
        {
            get { return scheduleName; }
        }

        public string[] CronTabs
        {
            get { return cronTabs; }
        }

        public ScheduleMethod ScheduleMethod
        {
            get { return scheduleMethod; }
        }

        public ScheduleInfo(string scheduleName, string[] cronTabs, ScheduleMethod scheduleMethod)
        {
            this.scheduleName = scheduleName;
            this.cronTabs = cronTabs;
            this.scheduleMethod = scheduleMethod;
        }
    }
}
