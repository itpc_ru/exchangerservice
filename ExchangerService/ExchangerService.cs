﻿using System;
using System.ServiceProcess;

namespace ExchangerService
{
    /// <summary>
    /// Инструкция по установке сервиса:
    /// 1. скопировать файлы сервиса (exe и dll-ки в отдельный каталог)
    /// 2. Зарегистрировать сервис согласно инструкции: https://msdn.microsoft.com/ru-ru/library/zt39148a(v=vs.110).aspx#BK_AddInstallers
    /// 3. Сервис должен запускаться от имени пользователя который может обращаться к внешним веб-сервисам
    /// </summary>
    public partial class ExchangerService : ServiceBase
    {
        private ExchangerBase ricExchanger = null;

        public ExchangerBase RICExchanger
        {
            get { return ricExchanger; }
        }

        public ExchangerService()
        {
            InitializeComponent();
        }

        public ExchangerService(ExchangerBase ricExchanger)
        {
            this.ricExchanger = ricExchanger;
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            OnStart();
        }

        public void OnStart()
        {
            try
            {
                ricExchanger.RunByTimer();
                eventLog1.WriteEntry("Service Started");
            }
            catch (Exception ex)
            {
                eventLog1.WriteEntry("Service Stopped");
                this.Stop();
            }
        }

        protected override void OnStop()
        {
            StopService();            
        }

        public void StopService()
        {
            if (ricExchanger != null) ricExchanger.Stop();
            eventLog1.WriteEntry("Service Stopped");
        }

        public static ExchangerService Create()
        {
            ExchangerService instance = null;
            try
            {
                instance = new ExchangerService();
                var eventLog = new EventLogAdapter(instance.eventLog1);
                instance.ricExchanger = Exchanger.Create(eventLog);
            }
            catch (Exception ex)
            {
                instance.eventLog1.WriteEntry("Error in RICService: " + ex.Message);
                instance.eventLog1.WriteEntry("Service Stopped");
                instance.Stop();
            }
            return instance;
        }
    }
}
