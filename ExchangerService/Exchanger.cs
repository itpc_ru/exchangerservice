﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;

namespace ExchangerService
{
    public class Exchanger : ExchangerBase
    {
        [ImportMany("ScheduleMethod")]
        public IEnumerable<ScheduleMethod> ScheduleMethods { get; set; }

        private EventLog eventLog;
        private int pollInterval;
        protected TimerBase timer;

        public static Exchanger Create(EventLog eventLog, TimerBase timer)
        {
            Exchanger instance = new Exchanger();
            instance.eventLog = eventLog;
            instance.timer = timer;
            instance.pollInterval = timer.PollInterval;
            new LoadSheduleMethodsCommand(instance).Execute();
            return instance;
        }

        public static Exchanger Create(EventLog eventLog)
        {
            Exchanger instance = null;
            int pollInterval = 5000;
            string iStr = ConfigurationManager.AppSettings["PollInterval"];
            if (!String.IsNullOrEmpty(iStr))
            {
                pollInterval = iStr.ToInt();
            }
            instance = Exchanger.Create(eventLog, new ServiceTimer(pollInterval));
            instance.timer.Elapsed += instance.timer_Elapsed;

            if (!String.IsNullOrEmpty(iStr))
            {
                instance.eventLog.WriteEntry("in App.config not found key [ServiceGisStartSendInterval] - on default 5 sec.");
            }
            return instance;
        }

        public void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            timer.Stop();
            Run(DateTime.Now);
            timer.Start();
        }

        public void RunByTimer()
        {
            timer.Start();
        }

        public void Run(DateTime now)
        {
            if (IsCanBeRunTasks.Make(now, pollInterval))
            {
                foreach (var method in ScheduleMethods)
                {
                    string resultMessage = method();
                    eventLog.WriteEntry(resultMessage);
                }
            }
        }

        public void Stop()
        {
            timer.Stop();
        }
    }
}
