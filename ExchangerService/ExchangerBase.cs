﻿namespace ExchangerService
{
    public interface ExchangerBase
    {
        void RunByTimer();
        void Stop();
    }
}
