﻿using System.Timers;

namespace ExchangerService
{
    public interface TimerBase
    {
        int PollInterval { get; }
        event ElapsedEventHandler Elapsed;
        void Start();
        void Stop();
    }
}
