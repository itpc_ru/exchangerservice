﻿namespace ExchangerService
{
    public class EventLogAdapter : EventLog
    {
        private System.Diagnostics.EventLog eventLog;

        public EventLogAdapter(System.Diagnostics.EventLog eventLog)
        {
            this.eventLog = eventLog;
        }

        public void WriteEntry(string message)
        {
            eventLog.WriteEntry(message);
        }
    }
}
