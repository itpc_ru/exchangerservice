﻿namespace ExchangerService
{
    public interface EventLog
    {
        void WriteEntry(string message);
    }
}
